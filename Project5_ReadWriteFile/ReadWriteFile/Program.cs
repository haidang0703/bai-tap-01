﻿using System;
using System.IO;
using System.Text;

/*
 https://docs.microsoft.com/en-us/troubleshoot/dotnet/csharp/read-write-text-file
 */
namespace ReadWriteFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            int a = 0, b = 0, t;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("C# Read/Write File!");
            ReadFile(ref a, ref b);
            t = a + b;
            str = "Tổng = " + t;
            WriteFile(str);
        }

        public static void ReadFile(ref int a, ref int b)
        {
            String line;
            try
            {
                StreamReader sr = new StreamReader("files/read.txt");
                line = sr.ReadLine();
                while (line != null)
                {
                    Console.WriteLine(line);
                    if (String.Compare(line.Substring(0,1), "a", true) == 0)
                    {
                        a = int.Parse(line.Substring(2).Trim());
                    }
                    else
                    {
                        b = int.Parse(line.Substring(2).Trim());
                    }
                    line = sr.ReadLine();
                }
                sr.Close();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

        static void WriteFile(string str)
        {
            try
            {
                StreamWriter sw = new StreamWriter("files/write.txt");
                sw.WriteLine(str);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }
    }
}
