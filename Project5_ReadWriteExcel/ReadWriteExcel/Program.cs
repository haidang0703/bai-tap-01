﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace ReadWriteExcel
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            ReadExcel();
            WriteExcel();
            Console.Read();

        }

        public static void ReadExcel()
        {
            
            Excel.Application excel = new Excel.Application();
            Excel.Workbook xlWorkbook = excel.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory +  "Import.xlsx");
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range excelRange = xlWorksheet.UsedRange;
            int rowCount = excelRange.Rows.Count;
            int colCount = excelRange.Columns.Count;
            for (int i = 1; i <= rowCount; i++)
            {
                for (int j = 1; j <= colCount; j++)
                {
                    if (j == 1)
                        Console.Write("\r\n");
                    if (excelRange.Cells[i, j] != null && excelRange.Cells[i, j].Value2 != null)
                        Console.Write(excelRange.Cells[i, j].Value2.ToString() + "\t");
                }
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Marshal.ReleaseComObject(excelRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);
            excel.Quit();
            Marshal.ReleaseComObject(excel);
            Console.WriteLine("Đã đọc file xong!");
        }

        public static void WriteExcel()
        {
            Excel.Application excel = new Excel.Application();
            if (excel != null)
            {
                Excel.Workbook excelWorkbook = excel.Workbooks.Add();
                Excel.Worksheet excelWorksheet = (Excel.Worksheet)excelWorkbook.Sheets.Add();

                excelWorksheet.Cells[1, 1] = "STT";
                excelWorksheet.Cells[1, 2] = "MSV";
                excelWorksheet.Cells[1, 3] = "Họ và Tên";
                excelWorksheet.Cells[1, 4] = "Lớp";

                excelWorksheet.Cells[2, 1] = "1";
                excelWorksheet.Cells[2, 2] = "1123456789";
                excelWorksheet.Cells[2, 3] = "Bùi Dương Thế";
                excelWorksheet.Cells[2, 4] = "CNTT";

                excel.ActiveWorkbook.SaveAs(AppDomain.CurrentDomain.BaseDirectory  + "Export_"+ DateTime.Now.ToString("dd_MMMM_hh_mm_ss_tt")  + ".xlsx", System.Reflection.Missing.Value,System.Reflection.Missing.Value,System.Reflection.Missing.Value,false,false,Excel.XlSaveAsAccessMode.xlShared,false,false,System.Reflection.Missing.Value,System.Reflection.Missing.Value,System.Reflection.Missing.Value);

                excelWorkbook.Close();
                excel.Quit();

                Marshal.FinalReleaseComObject(excelWorksheet);
                Marshal.FinalReleaseComObject(excelWorkbook);
                Marshal.FinalReleaseComObject(excel);

                GC.Collect();
                GC.WaitForPendingFinalizers();
                Console.WriteLine("Đã ghi file xong!");
            }
            else{
                Console.WriteLine("Lỗi ghi file!");
            }
        }
    }
}
