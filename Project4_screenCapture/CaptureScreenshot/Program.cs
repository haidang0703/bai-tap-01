﻿using System;
using System.Text;
using System.Drawing;
using System.Threading;

namespace CaptureScreenshot
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("Khởi động và chụp hình màn hình...");
            Console.WriteLine();
            Bitmap CSImage;
            CSImage = new Bitmap(3200, 1800);
            Size s = new Size(CSImage.Width, CSImage.Height);
            Graphics memoryGraphics = Graphics.FromImage(CSImage);
            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);
            string fileName = "Screenshot/Screenshot" + "_" + DateTime.Now.ToString("(dd_MMMM_hh_mm_ss_tt)") + ".png";
            CSImage.Save(fileName);
            Console.WriteLine("Ành đã lưu tại..." + fileName);
            Console.Read();
        }
    }
}
